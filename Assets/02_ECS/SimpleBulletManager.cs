﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Simple
{
    public class EXSScene : BulletManagerBase
    {

        [SerializeField]
        SimpleBullet _bulletPrefab = null;

        // SimpleBullet _bulletPrefab = null;

        List<SimpleBullet> _bulletPool = null;

        int _currentIndex = 0;

        protected override void Init(){
            base.Init();
            _bulletPool = new List<SimpleBullet>();
            //大量に作っておく
            for(int i=0; i<10000; ++i) {

                var bulletObj = Instantiate(_bulletPrefab);
                bulletObj.transform.SetParent(_bulletParent, false);

                var x = Random.Range(_bulletRange.x, _bulletRange.y);
                var bullet = bulletObj.GetComponent<SimpleBullet>();
                _bulletPool.Add(bullet);
                bullet.gameObject.SetActive(false);
            }
            _currentIndex = 0;
        }
        
        /// <summary>
        /// 弾の生成
        /// </summary>
        protected override void InstantiateBullet(Transform parent, Vector2 posXRange, float posY)
        {
            var bullet = _bulletPool[_currentIndex++];
            var x = Random.Range(_bulletRange.x, _bulletRange.y);
            bullet.Initialize(new Vector3(x, posY, 0.0f), DisplayOutBullet);
            bullet.gameObject.SetActive(true);
            if( _currentIndex >=  _bulletPool.Count) {
                _currentIndex = 0;
            }
        }


        void DisplayOutBullet(SimpleBullet bullet)
        {
            bullet.gameObject.SetActive(false);
        }

        

    }
}
