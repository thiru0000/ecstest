﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Simple {
    public class HotOnlyPlayer : MonoBehaviour
    {
        [SerializeField]
        SpriteRenderer _collisionSprite = null;

        // 移動スピード
        public bool _isHit = false;

        Color green = Color.green;
        Color red = Color.red;

        // Update is called once per frame
        void Update()
        {
            if( _isHit ) {
                _collisionSprite.color = red;
            } else {
                _collisionSprite.color = green;
            }
        }


        void OnTriggerEnter2D(Collider2D c)
        {
            _isHit = true;
        }

        void OnTriggerExit2D(Collider2D c)
        {
            _isHit = false;
        }

    }
}
