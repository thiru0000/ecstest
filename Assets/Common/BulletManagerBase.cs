﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class BulletManagerBase: MonoBehaviour
{
    [SerializeField]
    protected Transform _bulletParent = null;


    public int createQuantity { set{ _createQuantity = value; } }

    protected Vector2 _bulletRange = Vector2.zero;
    protected float _initialPosY = 0;
    protected int _createQuantity = 1;


    /// <summary>
    /// 弾の生成
    /// </summary>
    protected abstract void InstantiateBullet(Transform parent, Vector2 posXRange, float posY);


    protected virtual void Init(){
        var min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        var max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
        _bulletRange = new Vector2(min.x, max.x);
        _initialPosY = max.y;
    }
	
    void Start()
	{
        Init();
	}

	// Update is called once per frame
	void Update()
    {
        for (int i = 0; i < _createQuantity; ++i ){
            CreateBullet();    
        }

    }

    void CreateBullet()
    {
        InstantiateBullet(_bulletParent, _bulletRange, _initialPosY);
    }

}

