﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletCounter : MonoBehaviour {

    [SerializeField]
    Transform _bulletParent = null;

    [SerializeField]
    Text _text = null;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        _text.text = _bulletParent.childCount.ToString();
	}
}
