﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletInstantiateQuantityController : MonoBehaviour
{

    [SerializeField]
    Text _text = null;

    [SerializeField]
    int _addValue = 10;


    BulletManagerBase _bulletManager = null;

    int _quantity = 0;




    public void AddValue()
    {
        _quantity += _addValue;
        _bulletManager.createQuantity = _quantity;
        UpdateText();
    }


    void Start()
    {
        _bulletManager = FindObjectOfType<BulletManagerBase>();
        _bulletManager.createQuantity = _quantity;
        UpdateText();
    }

    void UpdateText()
    {
        _text.text = _quantity.ToString();
    }
}