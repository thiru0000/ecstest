﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Simple {
    public class Player : MonoBehaviour
    {

        // 移動スピード
        public float speed;

        // Update is called once per frame
        void Update()
        {
            float x = Input.GetAxisRaw("Horizontal");
            Vector2 direction = new Vector2(x, 0).normalized;
            Move(direction);
        }

        void Move(Vector2 direction)
        {
            Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
            Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
            Vector2 pos = transform.position;
            pos += direction * speed * Time.deltaTime;
            pos.x = Mathf.Clamp(pos.x, min.x, max.x);
            pos.y = Mathf.Clamp(pos.y, min.y, max.y);

            transform.position = pos;
        }

        void OnTriggerEnter2D(Collider2D c)
        {
            Destroy(gameObject);
        }
    }
}
