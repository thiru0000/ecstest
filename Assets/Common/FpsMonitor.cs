﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/**
 * フレームレート測定モニタ
 */
public class FpsMonitor : MonoBehaviour
{
    [SerializeField]
    private Text _text = null;

    //測定用
    int tick = 0;             //フレーム数
    float elapsed = 0;        //経過時間
    float fps = 0;            //フレームレート


    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        tick++;
        elapsed += Time.deltaTime;
        if (elapsed >= 1f)
        {
            fps = tick / elapsed;
            tick = 0;
            elapsed = 0;
            _text.text = fps.ToString("F1");
        }
    }
}
