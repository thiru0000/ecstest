﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Simple
{
    public class SimpleBullet : MonoBehaviour
    {
    	// 移動スピード
        public float speed;

        System.Action<SimpleBullet> _outDisplayCallBack = null;
        public void Initialize( Vector3 pos, System.Action<SimpleBullet> outDisplayCallBack ){
        	transform.position = pos;
            _outDisplayCallBack = outDisplayCallBack;
        }


        void Update()
        {
            Move(Vector2.down);
        }

        void Move(Vector2 direction)
        {
            Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));

            Vector2 pos = transform.position;
            pos += direction * speed * Time.deltaTime;
            transform.position = pos;

            //画面下を超えたら削除
            if( transform.position.y <  min.y ) {
                _outDisplayCallBack(this);
            }
        }

    }
}