﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Simple
{
    public class SampleGameBulletManager : BulletManagerBase
    {

        [SerializeField]
        SimpleBullet _bulletPrefab = null;

        /// <summary>
        /// 弾の生成
        /// </summary>
        protected override void InstantiateBullet(Transform parent, Vector2 posXRange, float posY)
        {
            var bulletObj = Instantiate(_bulletPrefab);
            bulletObj.transform.SetParent(parent, false);

            var x = Random.Range(posXRange.x, posXRange.y);
            var bullet = bulletObj.GetComponent<SimpleBullet>();

            bullet.Initialize(new Vector3(x, posY, 0.0f), (outBullet)=>{ Destroy(outBullet.gameObject); });
        }


    }
}
